﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication2
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
          
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.basictable.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery.mobile.custom.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/jquery.tools.min.js"));
            //-------------------------------------------------
            // js folder//
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
          "~/Scripts/bootstrap.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/easing.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/jquery-2.1.4.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/jquery.dotdotdot.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/jquery.flexslider.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/jquery.magnific-popup.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
        "~/Scripts/jquery.slidey.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
     "~/Scripts/simplePlayer.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
     "~/Scripts/owl.carousel.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
   "~/Scripts/move-top.js"));
            bundles.Add(new StyleBundle("~/Content/Css").Include("~/Content/bootstrap.css" , "~/Content/news1.css", "~/Content/table-style.css", "~/Content/basictable.css", "~/Content/list.css", "~/Content/style.css", "~/Content/single.css", "~/Content/popuo-box.css", "~/Content/owl.carousel.css", "~/Content/news.css", "~/Content/medile.css", "~/Content/bootstrap.css", "~/Content/jquery.slidey.min.css", "~/Content/font-awesome.min.css", "~/Content/flexslider.css", "~/Content/faqstyle.css", "~/Content/contactstyle.css"));

        }
    }
}
